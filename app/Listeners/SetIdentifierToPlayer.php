<?php

namespace App\Listeners;

use App\Events\IdentifyingPlayer;
use Ramsey\Uuid\Uuid;

/**
 * Class SetIdentifierToPlayer
 * @package App\Listeners
 */
class SetIdentifierToPlayer
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param IdentifyingPlayer $event
     */
    public function handle(IdentifyingPlayer $event)
    {
        $event->player->setAttribute('identifier', Uuid::uuid4());
    }
}
