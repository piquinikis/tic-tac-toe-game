<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'index');

Route::post('/broadcasting/auth', '\Illuminate\Broadcasting\BroadcastController@authenticate');

Route::prefix('api')->group(function () {
    Route::post('join', 'Auth\RegisterController@register')->name('join');
    Route::get('match', 'MatchController@matches')->name('matches');
    Route::get('match/{match}', 'MatchController@match')->name('match');
    Route::put('match/{match}', 'MatchController@move')->name('move');
    Route::put('match/join/{match}', 'MatchController@joinGame')->name('join_game');
    Route::put('match/left/{match}', 'MatchController@leftGame')->name('left_game');
    Route::post('match', 'MatchController@create')->name('create_match');
    Route::delete('match/{match}', 'MatchController@delete')->name('delete_match');
});