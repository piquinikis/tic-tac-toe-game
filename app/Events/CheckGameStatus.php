<?php

namespace App\Events;

use App\Match;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class CheckGameStatus
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Match
     */
    protected $match;

    /**
     * CheckGameStatus constructor.
     * @param Match $match
     */
    public function __construct(Match $match)
    {
        $this->match = $match;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('App.Games.' . $this->match->id);
    }
}
