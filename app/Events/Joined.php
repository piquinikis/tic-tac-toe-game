<?php

namespace App\Events;

use App\Match;
use App\PlayerJoined;

/**
 * Class Joined
 * @package App\Events
 */
class Joined
{
    /**
     * @var Match
     */
    public $match;

    /**
     * @var PlayerJoined
     */
    public $playerJoined;

    /**
     * Joined constructor.
     * @param Match $match
     * @param PlayerJoined $playerJoined
     */
    public function __construct(Match $match, PlayerJoined $playerJoined)
    {
        $this->match = $match;
        $this->playerJoined = $playerJoined;
    }

}
