<?php

namespace App\Http\Services;

use App\Events\Joined;
use App\Events\PlayerMove;
use App\Match;
use App\PlayerJoined;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\UnauthorizedException;

/**
 * Class GameService
 * @package App\Http\Services
 */
class GameService
{
    /**
     * @param Match $match
     * @param int $position
     */
    public function madeMovement(Match $match, int $position)
    {
        /** @var Collection $players */
        $players = $match->players()->getResults();
        $player = $players->where('player_id', '=', Auth::user()->getAuthIdentifier())->first();

        if (!$player->isYourTurn($match)){
            throw new UnauthorizedException();
        }

        event(new PlayerMove($match, $player, $position));
    }

    /**
     * @param Match $match
     * @param $input
     * @return PlayerJoined
     */
    public function joinAs(Match $match, $input)
    {
        $joined = new PlayerJoined([
            'player_id' => Auth::user()->getAuthIdentifier(),
            'play_as' => $input
        ]);

        $joined->match()->associate($match);

        event(new Joined($match, $joined));

        $joined->save();
        $match->save();

        return $joined;
    }

    /**
     * @param Match $match
     */
    public function left(Match $match)
    {
        PlayerJoined::query()
            ->where('match_id', '=', $match->id)
            ->where('player_id', '=', Auth::user()->getAuthIdentifier())
            ->delete();
    }
}