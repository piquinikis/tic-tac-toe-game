<?php

namespace App\Events;

use App\Match;

/**
 * Class CreateMatch
 * @package App\Events
 */
class CreateMatch
{
    /**
     * @var Match
     */
    public $match;

    /**
     * CreateMatch constructor.
     * @param Match $match
     */
    public function __construct(Match $match)
    {
        $this->match = $match;
    }
}
