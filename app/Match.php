<?php

namespace App;

use App\Events\CheckGameStatus;
use App\Events\CreateMatch;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Match
 * @package App
 */
class Match extends Model
{
    const BEST = 'best';
    const ONE = 'one';

    protected $table = 'matches';

    protected $fillable = ['name', 'type'];

    protected $casts = [
        'board' => 'array'
    ];

    protected $hidden = ['player_id'];

    protected $dispatchesEvents = [
        'creating' => CreateMatch::class,
        'saved' => CheckGameStatus::class
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function player()
    {
        return $this->belongsTo(Player::class, 'player_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function players()
    {
        return $this->hasMany(PlayerJoined::class, 'match_id', 'id');
    }

    /**
     * @return boolean
     */
    public function isEmptyRoom()
    {
        return $this->players()->count() === 0;
    }

    /**
     * @return Match
     */
    public function checkWinnerStatus()
    {
        $turn = $this->getAttribute('turn');
        /** @var Collection $players */
        $players = $this->players()->getResults();

        $player = $players->first(function (PlayerJoined $playerJoined) use ($turn) {
            if ($playerJoined->getAttribute('play_as') === $turn) {
                return $playerJoined->player()->getResults();
            }
        });

        $this->storeWinner($player);

        return $this;
    }

    /**
     * @return bool
     */
    public function isGameOver()
    {
        return !is_null($this->getAttribute('winner'));
    }

    /**
     * @return bool
     */
    public function isFull()
    {
        return $this->players()->count() === 2;
    }

    /**
     *
     */
    public function resetBoard()
    {
        $this->setAttribute('board', [0,0,0,0,0,0,0,0,0]);
    }

    /**
     * @param PlayerJoined $player
     */
    protected function storeWinner(PlayerJoined $player)
    {
        $player->setAsWinner();

        if ($this->type === self::ONE || $player->wins() == 2) {
            $this->setAttribute('winner', $player->getAttribute('id'));

            return;
        }
    }
}
