import axios from 'axios';

const URL_JOIN = '/api/join',
    URL_MATCHES = '/api/match',
    URL_MATCH = '/api/match/',
    URL_JOIN_GAME = '/api/match/join/',
    URL_LEFT_GAME = '/api/match/left/',
    URL_MOVE = '/api/match/',
    URL_CREATE = '/api/match',
    URL_DELETE = '/api/match/';

export default {
    checkMessage(response) {
        if ('message' in response) {
            alert(response.message);
        }
    },
    join: () => {
        return axios.post(URL_JOIN)
    },
    matches: () => {
        return axios.get(URL_MATCHES)
    },
    match: ({id}) => {
        return axios.get(URL_MATCH + id)
    },
    joinGame: ({id, playAs}) => {
        return axios.put(URL_JOIN_GAME + id, {
            playAs
        })
    },
    leftGame: (id) => {
        return axios.put(URL_LEFT_GAME + id, {})
    },
    move: ({id, position}) => {
        return axios.put(URL_MOVE + id, {
            position: position
        })
    },
    create: (data) => {
        return axios.post(URL_CREATE, data)
    },
    destroy: ({id}) => {
        return axios.delete(URL_DELETE + id)
    },
}