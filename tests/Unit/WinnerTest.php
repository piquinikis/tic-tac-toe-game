<?php

namespace Tests\Unit;

use App\Helpers\GameHelper;
use Tests\TestCase;

class WinnerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $boardLose = [
            [
                1, 1, 2,
                2, 1, 1,
                1, 2, 2
            ],
            [
                1, 1, 2,
                1, 1, 3,
                2, 2, 4
            ],
            [2,3],
            []
        ];

        $boardWin = [[
            1, 1, 1,
            2, 1, 2,
            1, 2, 2
        ],[
            1, 2, 1,
            2, 2, 2,
            1, 1, 2
        ],
        [
            1, 1, 2,
            2, 1, 1,
            1, 2, 1
        ],
        [
            2, 1, 2,
            2, 1, 1,
            2, 2, 1
        ]];

        foreach ($boardLose as $board) {
            $this->assertFalse(GameHelper::thereWinner($board));
        }

        foreach ($boardWin as $board) {
            $this->assertTrue(GameHelper::thereWinner($board));
        }
    }
}
