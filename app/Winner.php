<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Winner
 * @package App
 */
class Winner extends Model
{
    protected $table = 'winners';

    protected $fillable = ['player_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function player()
    {
        return $this->belongsTo(PlayerJoined::class);
    }
}