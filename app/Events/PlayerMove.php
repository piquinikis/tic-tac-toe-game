<?php

namespace App\Events;

use App\Match;
use App\PlayerJoined;

/**
 * Class PlayerMove
 * @package App\Events
 */
class PlayerMove
{

    /**
     * @var Match
     */
    public $match;

    /**
     * @var PlayerJoined
     */
    public $currentPlayer;

    /**
     * @var integer
     */
    public $newPosition;

    /**
     * PlayerMove constructor.
     * @param Match $match
     * @param PlayerJoined $currentPlayer
     * @param int $newPosition
     */
    public function __construct(Match $match, PlayerJoined $currentPlayer, int $newPosition)
    {
        $this->match = $match;
        $this->currentPlayer = $currentPlayer;
        $this->newPosition = $newPosition;
    }

}
