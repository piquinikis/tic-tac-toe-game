<?php

namespace App\Listeners;

use App\Events\PlayerMove;

/**
 * Class StoreTheMovement
 * @package App\Listeners
 */
class StoreTheMovement
{
    /**
     * @param PlayerMove $event
     */
    public function handle(PlayerMove $event)
    {
        $board = $event->match->getAttribute('board');
        $board[$event->newPosition] = $event->currentPlayer->getAttribute('play_as');

        $event->match->setAttribute('board', $board);
        $event->match->save();
    }
}
