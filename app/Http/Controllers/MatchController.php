<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMatch;
use App\Http\Resources\Matches;
use App\Http\Services\GameService;
use App\Match;
use Illuminate\Support\Facades\Input;

/**
 * Class MatchController
 * @package App\Http\Controllers
 */
class MatchController extends Controller
{
    /**
     * @var GameService
     */
    protected $gameService;

    /**
     * MatchController constructor.
     * @param GameService $gameService
     */
    public function __construct(GameService $gameService)
    {
        $this->gameService = $gameService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('index');
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function matches()
    {
        return Matches::collection(Match::all());
    }

    /**
     * @param Match $match
     * @return \Illuminate\Http\JsonResponse
     */
    public function match(Match $match)
    {
        return response()->json($match->load('players'));
    }

    /**
     * @param Match $match
     * @return \Illuminate\Http\JsonResponse
     */
    public function joinGame(Match $match)
    {
        if ($match->isFull()) {
            return response()->json(array_merge(['message' => 'Game full'], $match->toArray()));
        }

        if ($match->isGameOver()) {
            return response()->json(array_merge(['message' => 'Game over'], $match->toArray()));
        }

        $this->gameService->joinAs($match, Input::get('playAs'));

        return response()->json($match->load('players'));
    }

    /**
     * @param Match $match
     * @return \Illuminate\Http\JsonResponse
     */
    public function leftGame(Match $match)
    {
        $this->gameService->left($match);

        return response()->json($match);
    }

    /**
     * @param Match $match
     * @return \Illuminate\Http\JsonResponse
     */
    public function move(Match $match)
    {
        if ($match->isGameOver()) {
            return response()->json(array_merge(['message' => 'Game over'], $match->toArray()));
        }

        $position = Input::get('position');
        $this->gameService->madeMovement($match, $position);

        return response()->json($match);
    }

    /**
     * @param CreateMatch $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(CreateMatch $request)
    {
        $match = new Match($request->validated());
        $match->save();

        return response()->json($match);
    }

    /**
     * @param Match $match
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Match $match)
    {
        $match->delete();

        return response()->json($match);
    }

}