<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMatchType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('matches', function(Blueprint $table) {
            $table->string('type');
        });

        Schema::create('winners', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('player_id');

            $table->timestamps();
        });

        Schema::table('winners', function(Blueprint $table) {
            $table->foreign('player_id')->references('id')->on('players_joined')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('winners', function (Blueprint $table) {
            $table->dropForeign(['player_id']);
        });
    }
}
