<?php

namespace App\Listeners;

use App\Events\CreateMatch;
use Illuminate\Support\Facades\Auth;

/**
 * Class AssociatePlayer
 * @package App\Listeners
 */
class AssociatePlayer
{
    /**
     * @param CreateMatch $event
     */
    public function handle(CreateMatch $event)
    {
        $match = $event->match;

        $match->setAttribute('turn', 1);
        $match->resetBoard();

        $match->player()->associate(Auth::user());
    }
}
