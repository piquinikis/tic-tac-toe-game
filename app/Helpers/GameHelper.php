<?php

namespace App\Helpers;

/**
 * Class GameHelper
 * @package App\Helpers
 */
class GameHelper
{
    /**
     *
     */
    const WINNER_VARIANTS = [
        [0,1,2],
        [3,4,5],
        [6,7,8],
        [0,1,2],
        [0,4,8],
        [0,3,6],
        [1,4,7],
        [2,5,8],
        [2,4,6]
    ];

    /**
     * @param array $board
     * @return bool
     */
    public static function thereWinner(array $board)
    {
        $check = false;

        collect(self::WINNER_VARIANTS)->each(function($item) use ($board, &$check) {
            if (($board[$item[0]] > 0 && $board[$item[1]] > 0 && $board[$item[2]] > 0)
             && ($board[$item[0]] == $board[$item[1]] && $board[$item[0]] == $board[$item[2]])) {
                $check = true;

                return false;
            }
        });

        return $check;
    }
}