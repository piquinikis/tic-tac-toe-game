<?php

namespace App\Listeners;

use App\Events\PlayerMove;
use App\Helpers\GameHelper;
use App\Match;

/**
 * Class CheckWinnerStatus
 * @package App\Listeners
 */
class CheckWinnerStatus
{
    /**
     * @param PlayerMove $event
     */
    public function handle(PlayerMove $event)
    {
        $board = $event->match->getAttribute('board');

        if (GameHelper::thereWinner($board)) {
            $match = $event->match;

            if ($match->type === Match::BEST) {
                $match->resetBoard();
            }

            $match->checkWinnerStatus()->save();
        }
    }
}
