<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('matches', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('player_id')->nullable();
            $table->string('name');
            $table->text('board')->nullable();
            $table->integer('turn')->nullable();
            $table->boolean('winner')->nullable();
            $table->timestamps();
        });

        Schema::create('players', function(Blueprint $table) {
            $table->increments('id');
            $table->uuid('identifier');
            $table->timestamps();
        });

        Schema::create('players_joined',function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('match_id');
            $table->unsignedInteger('player_id');
            $table->integer('play_as');
            $table->timestamps();
        });

        Schema::table('matches', function (Blueprint $table) {
            $table->foreign('player_id')->references('id')->on('players')->onDelete('cascade');
        });

        Schema::table('players_joined', function (Blueprint $table) {
            $table->foreign('match_id')->references('id')->on('matches')->onDelete('cascade');
            $table->foreign('player_id')->references('id')->on('players')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('players_joined', function (Blueprint $table) {
            $table->dropForeign(['match_id', 'player_id']);
        });

        Schema::drop('matches', function (Blueprint $table) {
            $table->dropForeign(['player_id']);
        });

        Schema::drop('players');
    }
}
