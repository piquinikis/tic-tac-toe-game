<?php

namespace App\Listeners;

use App\Events\Joined;

/**
 * Class InitializeGame
 * @package App\Listeners
 */
class NewPlayerJoined
{
    /**
     * @param Joined $event
     */
    public function handle(Joined $event)
    {
        $match = $event->match;

        if ($match->isEmptyRoom()) {
            $match->setAttribute('turn', $event->playerJoined->getAttribute('play_as'));
            $match->save();
        }
    }
}
