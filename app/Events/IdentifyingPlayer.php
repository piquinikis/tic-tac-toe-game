<?php

namespace App\Events;

use App\Player;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

/**
 * Class IdentifyingPlayer
 * @package App\Events
 */
class IdentifyingPlayer
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Player
     */
    public $player;

    /**
     * IdentifyingPlayer constructor.
     * @param Player $player
     */
    public function __construct(Player $player)
    {
        $this->player = $player;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
