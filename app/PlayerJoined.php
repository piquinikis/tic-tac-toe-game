<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PlayerJoined
 * @package App
 */
class PlayerJoined extends Model
{
    protected $table = 'players_joined';

    protected $fillable = [
        'match_id',
        'player_id',
        'play_as',
        'your_turn'
    ];

    protected $casts = [
        'your_turn' => 'bool'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function match()
    {
        return $this->belongsTo(Match::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function player()
    {
        return $this->belongsTo(Player::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function winners()
    {
        return $this->hasMany(Winner::class, 'player_id', 'id');
    }

    /**
     * @return int
     */
    public function wins()
    {
        return $this->winners()->count();
    }

    /**
     * @param Match $match
     * @return bool
     */
    public function isYourTurn(Match $match)
    {
        return $this->getAttribute('play_as') === $match->getAttribute('turn');
    }

    /**
     *
     */
    public function setAsWinner()
    {
        $winner = new Winner(['player_id' => $this->id]);
        $winner->save();
    }
}
